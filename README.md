# TinyImage-V2.0.5

#### 介绍：
TinyImage  是一个轻量级的、开源的图像处理库，支持 bmp、jpeg、png、tiff(多页)图像的读写与转换，支持 1、4、8、16、24、32 位的图像及其不同位深之间的转换，支持黑白、1 位灰、4 位灰、8 位灰、8 位彩等 9 种颜色模式，支持50 多种基本的图像处理功能，支持超大图像，支持多页TIFF文件，支持多线程。


#### 软件架构说明：
![输入图片说明](https://images.gitee.com/uploads/images/2022/0305/141739_1609db23_10530611.png "屏幕截图.png")
上图是 TinyImage 的架构图。最外层是 C 风格的 API，目前一共有 68 个接口供用户使用；中间层是输入输出层，
主要负责图像文件的编解码，目前支持 bmp、jpeg、png、tiff 四种格式；最内层是 TinyImage 的内核，无论什么格式的图像，导入内核后，都统一解码成内存 DIB 图像，而且所有的图像处理算法也在该内核中。


#### 内存 DIB 图像格式说明：
TinyImage的图像在内存中以DIB的方式存储，数据在以下几种情况组织方式如下：

1.在调色板模式下。页面由调色板和图像数据两部分组成

2.支持1,4,8,16,24,32位图像，其中1,4,8为带调色板的图像，16，24,32为不带调色板的图像

3.16位图像，每个像素占两个字节，从低到高按B,G,R顺序排列，每个分量占5个bit

4.24位图像，每个像素占三个字节，从低到高按B,G,R顺序排列，每个分量占8个bit

5.32位图像，每个像素占四个字节，从低到高按B,G,R,A顺序排列，每个分量占8个bit

6.图像以左上角为原点


#### 调色板说明：
1.调色板每四个字节对应一个颜色，从低地址到高地址按照B,G,R,A的顺序排列

2.在位深BITS=1时，调色板共两个颜色，实际的图像数据部分每个字节包含八个像素的颜色索引

3.在位深BITS=4时，调色板共有16个颜色，实际的图像数据部分每个字节包含两个像素的颜色索引

4.在位深BITS=8时，调色板共有256个颜色，实际的图像数据部分每个字节包含一个像素的颜色索引



#### 图像数据说明：
1.为了支持超大图像，图像数据都是分块存储的

2.#define TINY_ROWSPERSTRIP 512；前面这个宏，定义了每个图像块的最大行数

3.如图像的高为1280，该图像将分成3块存储，第1块包含前面512行数据，第2块包含中间512行数据，第3块包含后面256行数据

4.为了字节对齐，每行数据的末尾都有冗余的无用数据；每行数据所占字节数的计算公式为

(((bpp * width + 31) >> 5) << 2)



#### V2.0.0更新内容：
1.支持超大图像，不再有内存限制，图像数据在内存中分块存储

2.接口设计更加合理，图像处理完毕后，返回新图像的句柄，旧图像仍旧可用

3.添加了DPI参数的获取和设置接口

4.在支持像素级数据读写的基础上，又添加了按行的方式的数据读写，每次可处理一行图像数据

5.添加了几个新的图像处理算法接口

6.把图像原点改成了左上角

7.添加了字符打印的接口

8.从win32升级到了x64



#### V2.0.1更新内容：
1.修改了Tiny_SetPixel的接口功能，使之对于调色板图像，能够自动修正传进来的颜色值

#### V2.0.2更新内容：
1.添加了对多页TIFF图像的读写功能

2.添加了Tiny_Init()和Tiny_Uninit()两个函数

3.给Tiny_Text()函数增加了水平打印或垂直打印的参数

4.给Tiny_Flood()函数增加了4领域填充或8领域填充的参数

#### V2.0.3更新内容：
1.修改了Tiny_Text()的接口功能，若传入的字体颜色和背景颜色相同，则背景颜色不起作用

2.添加了Tiny_Perspective()透视变换接口

3.添加了Tiny_MeanFilter()均值滤波接口

4.添加了Tiny_MedianFilter()中值滤波接口

5.添加了Tiny_Mix()图像融合接口

#### V2.0.4更新内容：
1.支持大于4G的TIFF文件的保存

2.添加了tiny_int和tiny_uint两个自定义类型

3.修改了Tiny_GetPalette()接口的功能，以便更方便访问调色板

#### V2.0.5更新内容：
1.添加了Tiny_Merge()图像拼接的接口

#### 接口说明：

	Tiny_Init
	Tiny_Uninit
	Tiny_GetPageNum
	Tiny_Load
	Tiny_Save
	Tiny_Create
	Tiny_Copy
	Tiny_Destroy
	Tiny_GetWidth
	Tiny_GetHeight
	Tiny_GetDPI
	Tiny_SetDPI
	Tiny_GetPixel
	Tiny_SetPixel
	Tiny_GetBpp
	Tiny_GetRowBytes
	Tiny_ReadRow
	Tiny_WriteRow
	Tiny_GetPaletteSize
	Tiny_GetPalette
	Tiny_SetPalette
	Tiny_Histogram
	Tiny_HistogramRGB
	Tiny_RotateLeft
	Tiny_RotateRight
	Tiny_Rotate180
	Tiny_FlipH
	Tiny_FlipV
	Tiny_Rotate
	Tiny_Crop
	Tiny_FillRect
	Tiny_Flood
	Tiny_Thumbnail
	Tiny_ResizeNearest
	Tiny_ResizeBilinear
	Tiny_ResizeBSpline
	Tiny_Gray
	Tiny_BinaryOtsu
	Tiny_BinaryST
	Tiny_BinaryBernsen
	Tiny_GaussianBlur
	Tiny_Sharpen
	Tiny_Robert
	Tiny_Prewitt
	Tiny_Sobel
	Tiny_Laplacian
	Tiny_Canny
	Tiny_Colorize
	Tiny_Brightness
	Tiny_Contrast
	Tiny_Saturate
	Tiny_Gamma
	Tiny_HistogramEqualize
	Tiny_HistogramStretch
	Tiny_Erode
	Tiny_Dilate
	Tiny_Skeleton
	Tiny_Thinning
	Tiny_ChangeBpp
	Tiny_Perspective
	Tiny_MeanFilter
	Tiny_MedianFilter
	Tiny_Mix
	Tiny_Merge
	Tiny_Point
	Tiny_Line
	Tiny_Ellipse
	Tiny_Text


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
